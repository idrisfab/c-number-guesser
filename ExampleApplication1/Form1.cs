﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Xml.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExampleApplication1
{
    public partial class Form1 : Form
    {
        // this will track the number of plays a player took 
        private static int playCount = 0;

        // this will store the name of the player.
        private static string userName = "";

        // this will hold the difficulty of the game string. easy medium and hard.
        private static string gameDifficulty;

        // create a new instance of the Random Class
        Random rand = new Random();
        int mysteryNumber;

        Color oldColor;

        public static string GameDifficulty
        {
            get
            {
                return gameDifficulty;
            }

            set
            {
                gameDifficulty = value;
            }
        }

        public static string UserName
        {
            get
            {
                return userName;
            }

            set
            {
                userName = value;
            }
        }

        public static int PlayCount
        {
            get
            {
                return playCount;
            }

            set
            {
                playCount = value;
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        public void setupNewGame(string difficulty)
        {
            // Reset the game
            lbl_guessResult.Text = "Guess the number";
            lbl_guessResult.BackColor = oldColor;

            lbl_userName.Text = UserName;

            lbl_answer.Hide();

            if (difficulty == "Easy")
            {
                mysteryNumber = rand.Next(1, 20);
            }
            else if (difficulty == "Medium")
            {
                mysteryNumber = rand.Next(1, 100);
            }
            else if (difficulty == "Hard")
            {
                mysteryNumber = rand.Next(1, 1000);
            }
            else // Then difficulty was not set properly
            {
                MessageBox.Show("Method not used correctly, you must only pass easy, medium or hard");
            }
            PlayCount = 0;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
             UserInfo usr = new UserInfo();
            usr.ShowDialog();
            setupNewGame(GameDifficulty);
        }

        private void btn_Guess_Click(object sender, EventArgs e)
        {
            //label2.Text = mysteryNumber.ToString();
            int guessNumber = 0; ;
            // This will get the number from the text box
            try
            {
                guessNumber = Convert.ToInt32(tbx_guess.Text);
                lbl_guessResult.Text = "Only enter an integer";
            }
            catch (System.FormatException ex)
            {
                MessageBox.Show("Error, only enter in an integer");
            }
            if( guessNumber > mysteryNumber)
            {
                lbl_guessResult.Text = guessNumber + " Your guess is too high";
            }
            else if( guessNumber < mysteryNumber)
            {
                lbl_guessResult.Text = guessNumber + " Your guess is too low";  
            }
            else
            {
                lbl_guessResult.Text = "Your guess is correct";
                lbl_guessResult.BackColor = Color.LightGreen;

                lbl_answer.Text = guessNumber.ToString();
                lbl_answer.Show();

                // little check to see if the player hit it in one!
                if(PlayCount == 1)
                {
                    // print message to say wow.
                    MessageBox.Show("WOW!! Thats a whole in one !, you guessed the right number first time!");
                }
                else
                {
                    // add a message box to state the number of moves it took and congratulate the player on winning.
                    MessageBox.Show("Well Done, " + UserName + " \nYou completed in " + (PlayCount +1) + " guesses");
                }
            }

            // clear the text box ready for new number
            tbx_guess.Text = "";

            // increment the playScore var
            PlayCount++;

            // write all gameinfo to a xml file
            saveInfoToXML();


        }

        private void tbx_guess_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13) // 13 IS THE ENTER KEY
            {
                btn_Guess.PerformClick();
            }
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserInfo usr = new UserInfo();
            usr.ShowDialog();
            setupNewGame(GameDifficulty);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 box = new AboutBox1();
            box.ShowDialog();
        }

        private void lbl_userName_Enter(object sender, EventArgs e)
        {
            
        }

        public void setupAGame()
        {
            oldColor = lbl_guessResult.BackColor;

            lbl_statusText.Text = "I have thought of a number...";

            // hide the answer label
            lbl_answer.Hide();
        }

        public void saveInfoToXML()
        {
            /*  this method will be create an instance of the information class, 
                the class will be serialised and written to an xml document.
                Data members in the class will be set first before the class is written 
                into xml.
            */
            try
            {
                // make a new instance of the information class
                Information info = new Information();

                // set the variables in the information class 
                info.Username = UserName;
                info.Time = "00:00";
                info.PlayCounter = (PlayCount+1).ToString();
                info.Score = 0;
                XmlData.SaveData(info, "fabData.xml");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void readInfoFromFile()
        {
            // firstly we need to check if the file we are going to write actually exists all ready.
            if(File.Exists("fabData.xml"))
            {
                XmlSerializer xs = new XmlSerializer(typeof(Information));
                FileStream read = new FileStream("fabData.xml", FileMode.Open, FileAccess.Read, FileShare.Read);

                // next we use the Deserialize method of the XmlSerializer class. The result must also be casted as Information.
                Information info = (Information) xs.Deserialize(read);


            }

        }

    }
}
