# README #

### What is this repository for? ###


Having not used visual studio for quite some time, I wanted to make a small application to test the basic concepts behind windows forms.
An exercise I like to complete with students is creating a number guessing program, something that will think of a random number and 
then the user will then guess the number with a simple high/low clue.

This is version one. I have created another branch to add some new features and test.

### How do I get set up? ###
I used visual Studio 2015 to create this. I made the entire project folder into the repo so it should be a simple case of cloning the repo to the 
visual studio workspace and then importing the project into Visual C#

### Who do I talk to? ###
   Idris Fab
   idris@fabiyi.com